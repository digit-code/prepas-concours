<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['throttle:60, 1'])->group(function () {
    Route::get('/', [App\Http\Controllers\WelcomeController::class, 'index'])->name('welcome');

    Route::get('/connexion', function () {
        return view('auth.login');
    })->name('connexion');

    Route::get('/inscription', function () {
        return view('auth.register');
    })->name('inscription');

    Route::get('/process', [App\Http\Controllers\ProcessController::class, 'index'])->name('process');
});

Route::middleware(['throttle:60, 1', 'auth'])->group(function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::get('/resultat', [App\Http\Controllers\ResultatController::class, 'index'])->name('result');

    Route::get('/store-error-reporting-data', [App\Http\Controllers\ResultatController::class, 'index'])->name('store-error-reporting-data');

    Route::get('/profile', [App\Http\Controllers\ProfileController::class, 'index'])->name('profile');
    Route::post('/update-profile', [App\Http\Controllers\ProfileController::class, 'update'])->name('update-profile');

    Route::get('/historique', [App\Http\Controllers\HistoriqueController::class, 'index_1'])->name('historique');

    Route::get('/classement', [App\Http\Controllers\ClassementController::class, 'index_1'])->name('classement');

    Route::get('/form-contact', [App\Http\Controllers\ContactController::class, 'create'])->name('form-contact');
    Route::post('/store-contact-data', [App\Http\Controllers\ContactController::class, 'store'])->name('store-contact-data');

    Route::get('/faq', [App\Http\Controllers\FaqController::class, 'index'])->name('faq');

    Route::get('/deconnexion', [App\Http\Controllers\LogoutController::class, 'perForm'])->name('logout.perForm');
});

Auth::routes();

Route::middleware(['throttle:60, 1', 'auth', 'role:Webmaster'])->group(function () {
    Route::get('/adminhomepage', [App\Http\Controllers\HomeController::class, 'index_1'])->name('adminhomepage');

    Route::get('/question', [App\Http\Controllers\QuestionController::class, 'index'])->name('question');
    Route::get('/form-create-question', [App\Http\Controllers\QuestionController::class, 'create'])->name('form-create-question');
    Route::post('/store-question', [App\Http\Controllers\QuestionController::class, 'store'])->name('store-question');
    Route::get('/form-edit-question/{id}', [App\Http\Controllers\QuestionController::class, 'edit'])->name('form-edit-question');
    Route::post('/update-question/{id}', [App\Http\Controllers\QuestionController::class, 'update'])->name('update-question');
    Route::get('/destroy-question/{id}', [App\Http\Controllers\QuestionController::class, 'destroy'])->name('destroy-question');

    Route::get('/user', [App\Http\Controllers\UserController::class, 'index'])->name('user');
    Route::get('/form-create-user', [App\Http\Controllers\UserController::class, 'create'])->name('form-create-user');
    Route::post('/store-user', [App\Http\Controllers\UserController::class, 'store'])->name('store-user');
    Route::get('/disabledOrActivateUserAccount/{id}', [App\Http\Controllers\UserController::class, 'disabledOrActivateUserAccount'])->name('disabledOrActivateUserAccount');
    Route::get('/form-edit-user/{id}', [App\Http\Controllers\UserController::class, 'edit'])->name('form-edit-user');
    Route::post('/update-user/{id}', [App\Http\Controllers\UserController::class, 'update'])->name('update-user');
    Route::get('/destroy-user/{id}', [App\Http\Controllers\UserController::class, 'destroy'])->name('destroy-user');

    Route::get('/classements', [App\Http\Controllers\ClassementController::class, 'index'])->name('classements');

    Route::get('/historiques', [App\Http\Controllers\HistoriqueController::class, 'index'])->name('historiques');
    Route::get('/historique/{id}', [App\Http\Controllers\HistoriqueController::class, 'show'])->name('show-historique');

    Route::get('/historique-de-connexion', [App\Http\Controllers\UserActivityLogController::class, 'index'])->name('historique-de-connexion');
    Route::get('/historique-de-connexion/{id}', [App\Http\Controllers\UserActivityLogController::class, 'show'])->name('show-historique-de-connexion');

    Route::get('/contact', [App\Http\Controllers\ContactController::class, 'index'])->name('contact');

    Route::post('/search', [App\Http\Controllers\SearchController::class, 'index'])->name('search');
});

Route::middleware(['throttle:60, 1', 'auth'])->group(function () {
    Route::get('/private', function () {
        return "This is private route";
    });

});

