<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EnsureUserHasRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, string $param1, string $param2 = '', string $param3 = '', string $param4 = '', string $param5 = '', string $param6 = '')
    {
        if ((auth::check()) and ((Auth::user()->role == $param1) or (Auth::user()->role == $param2) or (Auth::user()->role == $param3) or (Auth::user()->role == $param4) or (Auth::user()->role == $param5) or (Auth::user()->role == $param6))) {
            return $next($request);
        } else {
            // return redirect()->back()->with('error', "Vous n'etes pas autorisé à effectuer cette action");
            abort(401);
        }
        
        return $next($request);
    }
}
