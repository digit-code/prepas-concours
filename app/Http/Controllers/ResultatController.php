<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Historique;

class ResultatController extends Controller
{
    public function index()
    {
        $historique = Historique::select('point', 'created_at')
            ->where('historiques.user_id', '=', Auth::user()->id)
            ->latest()
            ->first();

        if($historique == NULL)
        {
            return redirect()->back()->with('error', "Oups!!! Une erreur inatendues s'est produit");
        }

        $results = Historique::join('questions', 'historiques.quest_id', '=', 'questions.id')
            ->where('historiques.user_id', '=', Auth::user()->id)
            ->select('historiques.choix','questions.quest', 'questions.rep1', 'questions.rep2', 'questions.rep3', 'questions.rep4', 'questions.brep')
            ->where('historiques.created_at', '=' ,$historique->created_at)
            ->get();

        return view('result.index', compact('historique' ,'results'));
    }
}
