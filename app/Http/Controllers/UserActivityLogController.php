<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserActivityLog;

class UserActivityLogController extends Controller
{
    public function index()
    {
        $logs = UserActivityLog::join('users', 'user_activity_logs.user_id', 'users.id')
            ->select('users.id as user_id','users.email', 'user_activity_logs.id as logID', 'user_activity_logs.ip_address', 'user_activity_logs.devise', 'user_activity_logs.mac_address', 'user_activity_logs.created_at', 'user_activity_logs.logout_at')
            ->orderByDesc('created_at')
            ->paginate(10);

        return view('historique_de_connexion.index', compact('logs'));
    }

    public function show($user_id)
    {
        $user = User::find($user_id);
        $logs = UserActivityLog::where('user_id', '=', $user_id)
            ->orderByDesc('created_at')
            ->paginate(10);

        return view('historique_de_connexion.show', compact('user' ,'logs'));
    }
}
