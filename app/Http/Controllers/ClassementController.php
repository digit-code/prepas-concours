<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class ClassementController extends Controller
{
    public function index_1()
    {
        $classements = User::select('id' ,'score', 'email')->orderBy('score', 'desc')->get();

        return view('classement.index_1', compact('classements'));
    }

    public function index()
    {
        $classements = User::select('id' , 'name','score', 'email')->orderBy('score', 'desc')->get();

        return view('classement.index', compact('classements'));
    }
}
