<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Question;
use App\Models\Historique;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $allQuestions = Question::count();
        
        $question_id = Historique::select('historiques.quest_id')->where('historiques.user_id', Auth::user()->id)->get();
        $quest_id = [];
        // tranformation en array
        foreach ($question_id as $value) {
            array_push($quest_id, $value->quest_id);
        }

        $questions = Question::join('users', 'questions.user_id', 'users.id')
            ->select('questions.quest', 'questions.rep1', 'questions.rep2', 'questions.rep3', 'questions.rep4', 'questions.brep', 'questions.created_at', 'users.email as creator_email', 'questions.id')
            ->whereNotIn('questions.id', $quest_id)
            ->orderBy('created_at', 'asc')
            ->limit(50)
            ->get();

        return view('home', compact('allQuestions' ,'questions'));
    }

    public function index_1()
    {
        return view('adminhomepage');
    }
}
