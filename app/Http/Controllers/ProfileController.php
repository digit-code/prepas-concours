<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class ProfileController extends Controller
{
    public function index()
    {
        $users = User::orderByDesc('score')->get();

        return view('profile.visiteur', compact('users'));
    }

    public function update(Request $request)
    {
        $check_password = Hash::check($request->input('old_password'), auth::user()->password);
        $profil = User::find(Auth::user()->id);
        if ($check_password) {
            if (($request->input('new_password') != NULL) or ($request->input('confirm_new_password') != NULL)) {
                if ($request->input('new_password') == $request->input('confirm_new_password')) {
                    $profil->password = $request->input('confirm_new_password');
                } else {
                    return redirect()->back()->with('error', 'Erreur. Les mots de pass ne correspondent pas.');
                }
            }
        } else {
            return redirect()->back()->with('error', 'Erreur. Mot de pass incorrect.');
        }
        $profil->name = $request->input('name');
        $profil->email = $request->input('email');
        $profil->update();

        return redirect()->back()->with('status', 'Information de profil mis à jour avec success.');
    }
}
