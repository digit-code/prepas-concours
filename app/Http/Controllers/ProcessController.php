<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Question;
use App\Models\Historique;

class ProcessController extends Controller
{
    public function index(Request $request)
    {
        if (Auth::check()) {
            if($request->input('rep') == NULL)
            {
                return redirect()->back()->with('error', 'Veuillez cochez au moins une case');
            }else{
                $score = 0;
                $point = 0;
                $ans = array();
                $ansId = array();
                $my_answer = array();
                foreach ($request->input('rep') as $key => $value)
                {
                    array_push($ans, [$key , json_encode($value)]);
                    array_push($ansId, $key);
                    array_push($my_answer, json_encode($value));
                }

                $db = Question::select('id', 'brep')->whereIn('questions.id', $ansId)->get();
                $dbAns = array();
                foreach ($db as $value) {
                    array_push($dbAns, [$value->quest_id, $value->brep]);
                }

                $date = now();
                for ($i=0; $i < count($ansId) ; $i++) { 
                    for ($j=0; $j < 2 ; $j++) { 
                        if (is_string($dbAns[$i][$j]) and is_string($ans[$i][$j])) {
                            if (str_replace(" ", "", $dbAns[$i][$j]) == $ans[$i][$j])
                            {
                                $score++;
                                $point = $point + 1;
                            }
                        }
                    }
                    if ((Historique::where('historiques.quest_id', '=', $ansId[$i])->count()) > 0) {
                        return redirect()->route('result')->with('status', 'success');
                    }else{

                        $historiques = new Historique();
                        $historiques->user_id = Auth::user()->id;
                        $historiques->quest_id = $ansId[$i];
                        $historiques->choix = $my_answer[$i];
                        $historiques->point = $point;
                        $historiques->created_at = $date;
                        $historiques->save();
                    }
                    $point = 0;
                }

                $user = User::find(Auth::user()->id);
                $user->score = $user->score + $score;
                $user->update();

                return redirect()->route('result')->with('status', 'success');
            }
        }else {
            return redirect()->route('connexion')->with('error', "Vous devriez vous connectez d'abord");
        }
    }
}
