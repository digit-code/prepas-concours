<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Historique;

class HistoriqueController extends Controller
{
    public function index()
    {
        $historiques = Historique::join('questions', 'historiques.quest_id', '=', 'questions.id')
            ->select('questions.id as quest_id', 'questions.quest', 'questions.rep1', 'questions.rep2', 'questions.rep3', 'questions.rep4', 'questions.brep', 'historiques.choix', 'historiques.point', 'historiques.created_at', 'historiques.user_id')
            ->orderByDesc('created_at')
            ->paginate(100);

        return view('historique.index', compact('historiques'));
    }

    public function index_1()
    {
        $historiques = Historique::join('questions', 'historiques.quest_id', '=', 'questions.id')
            ->where('historiques.user_id', '=', Auth::user()->id)
            ->select('questions.id as quest_id', 'questions.quest', 'questions.rep1', 'questions.rep2', 'questions.rep3', 'questions.rep4', 'questions.brep', 'historiques.choix', 'historiques.point', 'historiques.created_at', 'historiques.user_id')
            ->orderByDesc('created_at')
            ->paginate(50);

        return view('historique.index_1', compact('historiques'));
    }

    public function show($id)
    {
        $user = User::find($id);

        $historiques = Historique::join('questions', 'historiques.quest_id', '=', 'questions.id')
            ->where('historiques.user_id', '=', $id)
            ->select('questions.id as quest_id', 'questions.quest', 'questions.rep1', 'questions.rep2', 'questions.rep3', 'questions.rep4', 'questions.brep', 'historiques.choix', 'historiques.point', 'historiques.created_at', 'historiques.user_id')
            ->orderByDesc('created_at')
            ->paginate(50);

        return view('historique.show', compact('user' ,'historiques'));
    }
}
