<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Question;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        $key = trim($request->input('q'));
        
        if($key == NULL)
        {
            $users = array();
            $questions = array();
        }else{
            $users = User::query()
                ->where('name', 'like', "%{$key}%")
                ->orWhere('email', 'like', "%{$key}%")
                ->orWhere('type_de_compte', 'like', "%{$key}%")
                ->orWhere('role', 'like', "%{$key}%")
                ->orderByDesc('created_at')
                ->take(10)
                ->get();

            $questions = Question::query()
                ->join('users', 'questions.user_id', 'users.id')
                ->select('questions.quest', 'questions.rep1', 'questions.rep2', 'questions.rep3', 'questions.rep4', 'questions.brep', 'users.email as created_by', 'questions.created_at', 'questions.id')
                ->where('concours', 'like', "%{$key}%")
                ->where('question_de', 'like', "%{$key}%")
                ->orWhere('quest', 'like', "%{$key}%")
                ->orderByDesc('created_at')
                ->paginate(10);
        }


        return view('search.result', [
            'key' => $key,
            'users' => $users,
            'questions' => $questions
        ]);
    }
}
