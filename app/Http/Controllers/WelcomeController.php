<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Question;
use App\Models\Historique;

class WelcomeController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            $allQuestions = Question::count();
            
            $question_id = Historique::select('historiques.quest_id')->where('historiques.user_id', Auth::user()->id)->get();
            $quest_id = [];
            // tranformation en array
            foreach ($question_id as $value) {
                array_push($quest_id, $value->quest_id);
            }

            $questions = Question::join('users', 'questions.user_id', 'users.id')
                ->select('questions.quest', 'questions.rep1', 'questions.rep2', 'questions.rep3', 'questions.rep4', 'questions.brep', 'questions.created_at', 'users.email as creator_email', 'questions.id')
                ->whereNotIn('questions.id', $quest_id)
                ->orderBy('created_at', 'asc')
                ->limit(50)
                ->get();
        } else {
            $questions = Question::join('users', 'questions.user_id', 'users.id')
                ->select('questions.quest', 'questions.rep1', 'questions.rep2', 'questions.rep3', 'questions.rep4', 'questions.brep', 'questions.created_at', 'users.email as creator_email', 'questions.id')
                ->orderBy('created_at', 'asc')
                ->limit(50)
                ->get();
        }
        return view('welcome', compact('questions'));
    }
}
