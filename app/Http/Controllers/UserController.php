<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::OrderByDesc('created_at')->paginate(10);

        if ($request->input('sort') == 'croissant') {
            $users = User::OrderBy('name', 'asc')->paginate(10);
        }

        if ($request->input('sort') == 'nom') {
            $users = User::OrderBy('name', 'asc')->paginate(10);
        }

        if ($request->input('sort') == 'email'){
            $users = User::OrderBy('email', 'asc')->paginate(10);
        }

        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $checkpassword = Hash::check($request->input('new_password'), confirm_new_password);
        if ($request->input('new_password') == $request->input('confirm_new_password')) {
            $user = new User();
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->tel = $request->input('tel');
            $user->type_de_compte = $request->input('type_de_compte');
            $user->role = $request->input('role');
            $user->password = Hash::make($request->input('confirm_new_password'));
            $user->save();

            return redirect()->route('user')->with('status', 'Utilisateur creer avec success');
        }else{
            return redirect()->back()->with('error', 'Erreur. Les mots de pass ne se coincident pas');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function disabledOrActivateUserAccount($id)
    {
        $user = User::find($id);
        if ($user->is_disabled == 0) {
            $user->is_disabled = 1;
            $user->update();
            return redirect()->back()->with('status', 'Compte desactivé avec success');
        }
        if ($user->is_disabled == 1) {
            $user->is_disabled = 0;
            $user->update();
            return redirect()->back()->with('status', 'Compte activé avec success');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return view('user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if (($request->input('new_password') != NULL) or ($request->input('confirm_new_password') != NULL))
        {
            if ($request->input('new_password') == $request->input('confirm_new_password'))
            {
                $user->password = Hash::make($request->input('confirm_new_password'));
            }else{
                return redirect()->back()->with('error', 'Erreur. Les mots de pass ne correspondent pas');
            }
        }

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->tel = $request->input('tel');
        $user->type_de_compte = $request->input('type_de_compte');
        $user->role = $request->input('role');
        $user->update();

        return redirect()->route('user')->with('status', 'Utilisateur edité avec success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if ($user->role == 'Webmaster') {
            return redirect()->route('user')->with('error', 'Echec de suppression');
        }
        $user->delete();

        return redirect()->route('user')->with('status', 'Employé supprimer avec success');
    }
}
