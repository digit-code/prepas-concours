<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Question;
use App\Models\Reponse;
use App\Models\Brep;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $questions = Question::join('users', 'questions.user_id', 'users.id')
            ->select('questions.quest', 'questions.rep1', 'questions.rep2', 'questions.rep3', 'questions.rep4', 'questions.brep', 'questions.created_at', 'users.email as creator_email', 'questions.id')
            ->orderByDesc('created_at')
            ->paginate(10);

        if ($request->input('sort') == 'direct') {
            $questions = Question::join('users', 'questions.user_id', 'users.id')
                ->select('questions.quest', 'questions.rep1', 'questions.rep2', 'questions.rep3', 'questions.rep4', 'questions.brep', 'questions.created_at', 'users.email as creator_email', 'questions.id')
                ->where('concours', '=', 'direct')
                ->orderByDesc('created_at')
                ->paginate(10);
        }
        if ($request->input('sort') == 'professionel'){
            $questions = Question::join('users', 'questions.user_id', 'users.id')
                ->select('questions.quest', 'questions.rep1', 'questions.rep2', 'questions.rep3', 'questions.rep4', 'questions.brep', 'questions.created_at', 'users.email as creator_email', 'questions.id')
                ->where('concours', '=', 'professionel')
                ->orderByDesc('created_at')
                ->paginate(10);
        }

        return view('question.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('question.create', compact('request'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Question::where('quest', '=', $request->input('quest'))->exists())
        {
            return redirect()->back()->with('error', 'Oups!!! Cette question existe deja dans la base de donnees');
        }else{
            $question = new Question();
            $question->user_id = Auth::user()->id;
            $question->concours = $request->input('concours');
            $question->question_de = $request->input('question_de');
            $question->quest = $request->input('quest');
            $question->rep1 = $request->input('rep1');
            $question->rep2 = $request->input('rep2');
            $question->rep3 = $request->input('rep3');
            $question->rep4 = $request->input('rep4');
            $question->brep = json_encode($request->input('brep'));
            $question->save();

            return redirect()->route('question')->with('status', 'question ajouter au test qcm avec succes');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $question = Question::find($id);

        return view('question.edit', compact('question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $question = Question::find($id);
        // $question->concours = $request->input('concours');
        // $question->question_de = $request->input('question_de');
        $question->quest = $request->input('quest');
        $question->rep1 = $request->input('rep1');
        $question->rep2 = $request->input('rep2');
        $question->rep3 = $request->input('rep3');
        $question->rep4 = $request->input('rep4');
        $question->brep = json_encode($request->input('brep'));
        $question->update();

        return redirect()->route('question')->with('status', 'question editer avec succes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $question = Question::find($id)->delete();

        return redirect()->back()->with('status', 'question supprimer avec succes');
    }
}
