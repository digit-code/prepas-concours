<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class Contact extends Model
{
    use HasFactory;
    protected $fillable = ['email', 'sujet', 'msg', 'mailMsg', 'created_at', 'updated_at', 'deleted_at'];
}
