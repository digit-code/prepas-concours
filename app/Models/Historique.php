<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class Historique extends Model
{
    use softDeletes;

    protected $fillable = [
        'user_id', 
        'quest_id', 
        'choix', 
        'point', 
        'created_at', 
        'updated_at', 
        'deleted_at'
    ];
}
