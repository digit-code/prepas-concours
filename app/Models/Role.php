<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class Role extends Model
{
    use softDeletes;

    protected $fillable = ['des', 'desc', 'created_at', 'updated_at', 'deleted_at'];
}
