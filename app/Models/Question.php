<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class Question extends Model
{
    use softDeletes;

    protected $fillable = [
        'user_id', 
        'concours', //direct ou professionel
        'question_de',  //culture generale, de mathematique, detc
        'quest', 
        'rep1', 
        'rep2', 
        'rep3', 
        'rep4', 
        'brep', 
        'created_at', 
        'updated_at', 
        'deleted_at'
    ];
}
