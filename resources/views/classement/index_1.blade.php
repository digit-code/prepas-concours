@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center mb-3">
        <div class="col-6">
            <div class="card">
                <div class="card-body table-responsive">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if(count($classements) == 0)
                        <span class="text-danger">{{ __('No Data') }}</span>
                    @else
                    <table class="table table-sm table-striped">
                        <thead>
                            <tr>
                                <th scope="col">{{ __('Rang') }}</th>
                                <th scope="col">{{ __('Score') }}</th>
                                <th scope="col">{{ __('Adress mail') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($classements as $classement)
                                @if ($classement->id == auth::user()->id)
                                    <tr class="table-danger">
                                        <th scope="row">{{$loop->iteration}}</th>
                                        <td>{{$classement->score}}</td>
                                        <td>{{$classement->email}}</td>
                                    </tr>
                                @else
                                    <tr>
                                        <th scope="row">{{$loop->iteration}}</th>
                                        <td>{{$classement->score}}</td>
                                        <td>{{$classement->email}}</td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>

        </div>
    </div>


</div>
@endsection
