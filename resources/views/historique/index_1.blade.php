@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 mb-3">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger" role="alert">
                    {{ session('error') }}
                </div>
            @endif
        </div>


        <div class="col-md-12 mb-3">
            <div class="card">
                <div class="card-header">{{ __("Historique") }}</div>
                <div class="card-body table-responsive">
                    @if(count($historiques) == 0)
                        <span class="text-danger">{{ __('No Data') }}</span>
                    @else
                        <table class="table table-sm table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">{{ __('#') }}</th>
                                    <th scope="col">{{ __('Questions') }}</th>
                                    <th scope="col">{{ __('Réponse a') }}</th>
                                    <th scope="col">{{ __('Réponse b') }}</th>
                                    <th scope="col">{{ __('Réponse c') }}</th>
                                    <th scope="col">{{ __('Réponse d') }}</th>
                                    <th scope="col">{{ __('Mes choix') }}</th>
                                    <th scope="col">{{ __('Bonne(s) réponse(s)') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($historiques as $historique)
                                    <tr>
                                        <th scope="row">{{$loop->iteration}}</th>
                                        <td>{{$historique->quest}}</td>
                                        <td>{{$historique->rep1}}</td>
                                        <td>{{$historique->rep2}}</td>
                                        <td>{{$historique->rep3}}</td>
                                        <td>{{$historique->rep4}}</td>
                                        <td>{{$historique->choix}}</td>
                                        <td>{{$historique->brep}} <a href="{{ route('store-error-reporting-data', $historique->quest_id) }}">{{ __('reporting') }}</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <div class="mt-5 mb-3">{{$historiques->onEachSide(1)->links()}}</div>

                    @endif
                </div>
            </div>

        </div>

    </div>
</div>
@endsection
