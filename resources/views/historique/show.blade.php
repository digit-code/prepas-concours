@extends('layouts.app2')

@section('content')
<div class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">
            <span>{{ __("Historique de jeux de") }} {{ $user->name }} {{ __(":") }} {{ $user->email }} </span> 
            <small class="small">{{ __('( score actuelle : ')}} {{ $user->score }} {{ __('point )')}}</small>
        </h1>
    </div>

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger" role="alert">
            {{ session('error') }}
        </div>
    @endif

    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th scope="col">{{ __('#') }}</th>
                    <th scope="col">{{ __('Questions') }}</th>
                    <th scope="col">{{ __('Réponse a') }}</th>
                    <th scope="col">{{ __('Réponse b') }}</th>
                    <th scope="col">{{ __('Réponse c') }}</th>
                    <th scope="col">{{ __('Réponse d') }}</th>
                    <th scope="col">{{ __('Bonne(s) réponse(s)') }}</th>
                    <th scope="col">{{ __('Mes choix') }}</th>
                    <th scope="col">{{ __('Point') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($historiques as $historique)
                    <tr>
                        <th scope="row">{{$loop->iteration}}</th>
                        <td>{{$historique->quest}}</td>
                        <td>{{$historique->rep1}}</td>
                        <td>{{$historique->rep2}}</td>
                        <td>{{$historique->rep3}}</td>
                        <td>{{$historique->rep4}}</td>
                        <td>{{$historique->brep}}</td>
                        <td>{{$historique->choix}}</td>
                        <td>{{$historique->point}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <div class="mt-5 mb-3">{{$historiques->onEachSide(1)->links()}}</div>

    </div>
</div>
@endsection
