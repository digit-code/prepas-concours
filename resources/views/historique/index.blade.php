@extends('layouts.app2')

@section('content')
<div class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __("Historique des jeux des utilisateurs en temps réel") }}</h1>
    </div>

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger" role="alert">
            {{ session('error') }}
        </div>
    @endif

    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th scope="col">{{ __('#') }}</th>
                    <th scope="col">{{ __('Questions') }}</th>
                    <th scope="col">{{ __('Réponse a') }}</th>
                    <th scope="col">{{ __('Réponse b') }}</th>
                    <th scope="col">{{ __('Réponse c') }}</th>
                    <th scope="col">{{ __('Réponse d') }}</th>
                    <th scope="col">{{ __('Bonne(s) réponse(s)') }}</th>
                    <th scope="col">{{ __('Mes choix') }}</th>
                    <th scope="col">{{ __('Point') }}</th>
                    <th scope="col">{{ __('Action') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($historiques as $historique)
                    <tr>
                        <th scope="row">{{$loop->iteration}}</th>
                        <td>{{$historique->quest}}</td>
                        <td>{{$historique->rep1}}</td>
                        <td>{{$historique->rep2}}</td>
                        <td>{{$historique->rep3}}</td>
                        <td>{{$historique->rep4}}</td>
                        <td>{{$historique->brep}}</td>
                        <td>{{$historique->choix}}</td>
                        <td>{{$historique->point}}</td>
                        <td class="btn-group">
                            <a class="btn btn-sm btn-outline-secondary" href="{{ route('show-historique', $user->id) }}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clock-fill" viewBox="0 0 16 16">
                                    <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z"/>
                                </svg>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <div class="mt-5 mb-3">{{$historiques->onEachSide(1)->links()}}</div>

    </div>
</div>
@endsection
