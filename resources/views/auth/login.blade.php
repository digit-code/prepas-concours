@extends('layouts.app')

@section('content')
<div class="container">
    <div class="text-center">
        <main class="form-signin w-100 m-auto">
            <form method="POST" action="{{ route('login') }}">
                @csrf

                <img class="mb-4" src="../fichiers/logo/bootstrap-logo.svg" alt="" width="72" height="57">
                <h1 class="h3 mb-3 fw-normal">{{ __('Connecter vous svp')}}</h1>

                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ session('error') }}
                    </div>
                @endif
            
                <div class="form-floating">
                    <input type="email" class="form-control @error('email') is-invalid @enderror" id="floatingInput email" placeholder="name@example.com" name="email" value="{{ old('email') }}" autocomplete="email" autofocus>
                    <label for="floatingInput">{{ __('Address Mail') }}</label>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-floating">
                    <input type="password" class="form-control @error('email') is-invalid @enderror" id="floatingPassword email" placeholder="name@example.com" name="password" value="{{ old('password') }}" autocomplete="password" autofocus>
                    <label for="floatingPassword">{{ __('Mot de pass') }}</label>
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            
                <div class="checkbox mb-3">
                    <label>
                        <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <span> {{ __('Remember me') }} </span>
                    </label>
                </div>
                <a href="{{ route('password.request') }}">{{ __('Mot de pass oubliez?') }}</a>
                <button class="w-100 btn btn-lg btn-primary" type="submit">{{ __('Connexion') }}</button>
                <div class="d-flex">
                    <hr class="w-100">
                    <span> {{ __('ou') }} </span>
                    <hr class="w-100">
                </div>
                <a href="{{ route('inscription') }}">{{ __('Inscription') }}</a>
            </form>
        </main>
    </div>



</div>
@endsection
