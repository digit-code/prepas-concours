@extends('layouts.app2')

@section('content')
<div class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __("Formulaire d'edition des utilisateurs") }}</h1>
    </div>

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger" role="alert">
            {{ session('error') }}
        </div>
    @endif

    <form action="{{ route('update-user', $user->id) }}" method="post">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-3 mb-3">
                <label for="name">{{ __('Nom') }}</label>
                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}" required autocomplete="name" autofocus>
            </div>

            <div class="col-md-3 mb-3">
                <label for="email">{{ __('Address Mail') }}</label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" required autocomplete="email">
            </div>

            <div class="col-md-3 mb-3">
                <label for="tel">{{ __('Telephone') }}</label>
                <input id="tel" type="text" class="form-control @error('tel') is-invalid @enderror" name="tel" value="{{ $user->tel }}" required autocomplete="tel" autofocus>
            </div>

            <div class="col-md-3 mb-3">
                <label for="type_de_compte">{{ __('Type de compte') }}</label>
                <select class="form-select" name="type_de_compte" id="type_de_compte" required>
                    @if ($user->type_de_compte == 'Direct')
                        <option value="{{ __('Direct') }}" selected>{{ __('Direct') }}</option>
                    @else
                        <option value="{{ __('Direct') }}">{{ __('Direct') }}</option>
                    @endif

                    @if ($user->type_de_compte == 'Professionnel')
                        <option value="{{ __('Professionnel') }}" selected>{{ __('Professionnel') }}</option>
                    @else
                        <option value="{{ __('Professionnel') }}">{{ __('Professionnel') }}</option>
                    @endif

                    @if ($user->type_de_compte == 'Autre')
                        <option value="{{ __('Autre') }}" selected>{{ __('Autre') }}</option>
                    @else
                        <option value="{{ __('Autre') }}">{{ __('Autre') }}</option>
                    @endif
                </select>
            </div>

            <div class="col-md-3 mb-3">
                <label for="role">{{ __('Role') }}</label>
                <select class="form-select" name="role" id="role" required>
                    <option value="{{ __('Visiteur') }}">{{ __('Visiteur') }}</option>
                    <option value="{{ __('Analyste') }}">{{ __('Analyste') }}</option>
                    <option value="{{ __('Moderateur') }}">{{ __('Moderateur') }}</option>
                    <option value="{{ __('Editeur') }}">{{ __('Editeur') }}</option>
                    <option value="{{ __('Administrateur') }}">{{ __('Administrateur') }}</option>
                    <option value="{{ __('Webmaster') }}">{{ __('Webmaster') }}</option>
                </select>
            </div>

            <div class="col-md-3 mb-3">
                <label for="new_password">{{ __('Nouveau mot de pass') }}</label>
                <input id="new_password" type="password" class="form-control @error('new_password') is-invalid @enderror" name="new_password" placeholder="Optionnel" autocomplete="new-password">
            </div>

            <div class="col-md-3 mb-3">
                <label for="confirm_new_password">{{ __('Confirmer le nouveau mot de pass') }}</label>
                <input id="password-confirm" type="password" class="form-control" name="confirm_new_password" placeholder="Optionnel" autocomplete="confirm_new_password">
            </div>

            <div class="col-md-12 mb-3">
                <button class="btn btn-success" type="submit">{{ __('Créer') }}</button>
            </div>
        </div>
    </form>
</div>
@endsection
