@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 mb-3">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger" role="alert">
                    {{ session('error') }}
                </div>
            @endif
        </div>

        <div class="col-md-12 mb-3">
            <div class="row">
                <div class="col-md-8 col-lg-8">
                    <h4 class="mb-3">{{ __("Comment s'incrire ?") }}</h4>
                    <h4 class="mb-3">{{ __("Comment renitialiser son score") }}</h4>
                    <h4 class="mb-3">{{ __("Comment") }}</h4>
                    
                </div>

                @include('include.commentaire')

            </div>
        </div>

    </div>
</div>
@endsection
