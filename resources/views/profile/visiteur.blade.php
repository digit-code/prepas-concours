@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 mb-3">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger" role="alert">
                    {{ session('error') }}
                </div>
            @endif
        </div>

        <div class="col-md-12 mb-3">
            <div class="row">
                <div class="col-md-8 mb-3">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <h5 class="card-title">{{ __('Mon profil') }}</h5>
                            <div class="card-text">
                                <div class="rounded bg-body shadow-sm mb-3 p-3">
                                    @php
                                        $date = \Carbon\Carbon::parse(auth::user()->created_at)->formatLocaliZed('%Y-%m-%d');
                                        $heure = \Carbon\Carbon::parse(auth::user()->created_at)->formatLocaliZed('%H:%M:%S');
                                    @endphp
                                    {{ __('Vous avez rejoint PREPAS-CONCOURS le') }} {{ $date }} 
                                    {{ __('à') }} {{ $heure }} {{ __('.') }}
                                    {{ __('Vous avez totalisez') }} {{ auth::user()->score }} 
                                    {{ __('point dont') }} 
                                    {{ App\Models\Historique::where('user_id', '=', auth::user()->id)->where('point', '=', 1)->count() }} 
                                    {{ __('bonnes réponses trouvés sur') }} 
                                    {{ App\Models\Historique::where('user_id', '=', auth::user()->id)->count() }}
                                    {{ __('questions répondus et vous etes classé') }} 
                                    @foreach ($users as $user)
                                        @if ($user->id == Auth::user()->id)
                                            {{-- {{ $loop->iteration }} --}}
                                            @php
                                                $rang = $loop->iteration
                                            @endphp
                                        @endif
                                    @endforeach
                                    @if ($rang == 1)
                                        <span class="text-danger"> {{ $rang }}{{ __('er') }} </span>
                                    @else
                                        <span class="text-danger">{{ $rang }} {{ __('éme') }} </span>
                                    @endif
                                    {{ __('sur') }} {{ App\Models\User::count() }} {{ __('utilisateur') }}
                                </div>
                                <form method="POST" action="{{ route('update-profile') }}" enctype="multipart/form-data">
                                    @csrf
                                        <div class="mb-3">
                                            <label for="name">{{ ('Nom') }}</label>
                                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ auth::user()->name }}" required autocomplete="name" autofocus>
                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="mb-3">
                                            <label for="email">{{ ('Address Mail') }}</label>
                                            <input type="email" class="form-control @error('email') is-invalid @enderror" id="floatingInput email" placeholder="name@example.com" name="email" value="{{ auth::user()->email }}" autocomplete="email" autofocus>
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="mb-3">
                                            <label for="old_password">{{ ('Mot de pass') }}</label>
                                            <input id="old_password" type="password" class="form-control @error('old_password') is-invalid @enderror" name="old_password" required autocomplete="new-password">
                                            @error('old_password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="mb-3">
                                            <label for="new_password">{{ ('Nouveau mot de pass') }}</label>
                                            <input class="form-control" type="password" name="new_password" id="new_password" placeholder="Optionel">
                                        </div>

                                        <div class="mb-3">
                                            <label for="confirm_new_password">{{ ('Nouveau mot de pass') }}</label>
                                            <input class="form-control" type="password" name="confirm_new_password" id="confirm_new_password" placeholder="Optionel">
                                        </div>

                                        <button class="btn btn-outline-success" type="submit">{{ __('Mettre à jour') }}</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                @include('include.commentaire')

            </div>
        </div>

    </div>
</div>
@endsection
