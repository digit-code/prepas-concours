@extends('layouts.app2')

@section('content')
<div class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __("Tableau de bord") }}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="card p-2 me-2 mb-2">
                <div class="input-group d-inline">
                    <button type="button" class="btn btn-light" disabled>{{ __('Online') }}</button>
                    <button type="button" class="btn btn-light" disabled>{{ App\Models\UserActivityLog::where('logout_at', '=', NULL)->count() }}</button>
                </div>
            </div>
            <div class="card p-2 me-2 mb-2">
                <div class="input-group d-inline">
                    <button type="button" class="btn btn-light" disabled>{{ __('Total connected') }}</button>
                    <button type="button" class="btn btn-light" disabled>{{ App\Models\UserActivityLog::count() }}</button>
                </div>
            </div>
            <div class="card p-2 me-2 mb-2">
                <div class="input-group d-inline">
                    <button type="button" class="btn btn-light" disabled>{{ __('User') }}</button>
                    <button type="button" class="btn btn-light" disabled>{{ App\Models\User::count() }}</button>
                </div>
            </div>
        </div>
    </div>

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger" role="alert">
            {{ session('error') }}
        </div>
    @endif

    <h2>{{ __('Questions') }}</h2>
</div>
@endsection
