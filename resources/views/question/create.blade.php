@extends('layouts.app2')

@section('content')
<div class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __("Formulaire d'ajout de nouvelle question") }}</h1>
    </div>

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger" role="alert">
            {{ session('error') }}
        </div>
    @endif

    <form class="mb-5 bg-body" method="POST" action="{{ route('store-question') }}">
        @csrf

        <div class="row">
            <input id="concours" type="hidden" class="form-control @error('concours') is-invalid @enderror" name="concours" value="{{ $request->input('create') }}" required autocomplete="concours" autofocus>

            <div class="col-md-12 mb-3">
                <label for="quest">{{ __('Question') }}</label>
                <textarea name="quest" id="quest" class="form-control" cols="30" rows="5" required></textarea>
            </div>

            <div class="col-md-6 mb-3">
                <label for="rep1">{{ __('Réponse a') }}</label>
                <input id="rep1" type="text" class="form-control @error('rep1') is-invalid @enderror" name="rep1" value="{{ old('rep1') }}" required autocomplete="rep1" autofocus>
            </div>

            <div class="col-md-6 mb-3">
                <label for="rep2">{{ __('Réponse b') }}</label>
                <input id="rep2" type="text" class="form-control @error('rep2') is-invalid @enderror" name="rep2" value="{{ old('rep2') }}" required autocomplete="rep2" autofocus>
            </div>

            <div class="col-md-6 mb-3">
                <label for="rep3">{{ __('Réponse c') }}</label>
                <input id="rep3" type="text" class="form-control @error('rep3') is-invalid @enderror" name="rep3" value="{{ old('rep3') }}" required autocomplete="rep3" autofocus>
            </div>

            <div class="col-md-6 mb-3">
                <label for="rep4">{{ __('Réponse d') }}</label>
                <input id="rep4" type="text" class="form-control @error('rep4') is-invalid @enderror" name="rep4" value="{{ old('rep4') }}" required autocomplete="rep4" autofocus>
            </div>

            <div class="col-md-6 mb-3">
                <label for="brep">{{ __('Bonne(s) réponse(s)') }}</label>
                <select id="brep" class="form-select @error('rep4') is-invalid @enderror" name="brep[]" multiple required>
                    <option value="a">{{ __('Réponse a') }}</option>
                    <option value="b">{{ __('Réponse b') }}</option>
                    <option value="c">{{ __('Réponse c') }}</option>
                    <option value="d">{{ __('Réponse d') }}</option>
                </select>
            </div>

            <div class="col-md-6 mb-3">
                <label for="question_de">{{ __('Question de') }}</label>
                <select class="form-select" name="question_de" id="question_de" required>
                    <option value="Culture generale">{{ __('Culture generale') }}</option>
                    <option value="Mathematique">{{ __('Mathematique') }}</option>
                    <option value="Autre">{{ __('Autre') }}</option>
                </select>
            </div>

            <div class="col-md-12 mb-3">
                <button class="btn btn-success" type="submit">{{ __('Ajouter') }}</button>
                <button class="btn btn-secondary" type="reset">{{ __('Effacer') }}</button>
            </div>
        </div>
    </form>
</div>
@endsection
