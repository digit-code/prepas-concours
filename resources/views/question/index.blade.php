@extends('layouts.app2')

@section('content')
<div class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __("Liste des questions") }}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="dropdown">
                <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle" data-bs-toggle="dropdown"><span data-feather="calendar" class="align-text-bottom"></span>{{('Ajouter')}}</button>
                <ul class="dropdown-menu">
                    <li>
                        <form method="GET" action="{{ route('form-create-question') }}">
                            <input type="hidden" name="create" value="direct">
                            <button type="submit" class="dropdown-item w-100 btn btn-light">{{ __('Direct') }}</button>
                        </form>
                    </li>
                    <li>
                        <form method="GET" action="{{ route('form-create-question') }}">
                            <input type="hidden" name="create" value="professionel">
                            <button type="submit" class="dropdown-item w-100 btn btn-light">{{ __('Professionel') }}</button>
                        </form>
                    </li>
                </ul>
            </div>

            <div class="dropdown mx-2">
                <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle" data-bs-toggle="dropdown">
                    <span data-feather="calendar" class="align-text-bottom"></span>
                        {{('Trier par')}}
                </button>
                <ul class="dropdown-menu">
                    <li>
                        <form method="GET" action="{{ route('question') }}">
                            <input type="hidden" name="sort" value="direct">
                            <button type="submit" class="dropdown-item w-100 btn btn-light">{{ __('Concours direct') }}</button>
                        </form>
                    </li>
                    <li>
                        <form method="GET" action="{{ route('question') }}">
                            <input type="hidden" name="sort" value="professionel">
                            <button type="submit" class="dropdown-item w-100 btn btn-light">{{ __('Concours professionel') }}</button>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger" role="alert">
            {{ session('error') }}
        </div>
    @endif

    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th scope="col">{{ __('#') }}</th>
                    <th scope="col">{{ __('Question') }}</th>
                    <th scope="col">{{ __('Réponse 1') }}</th>
                    <th scope="col">{{ __('Réponse 2') }}</th>
                    <th scope="col">{{ __('Réponse 3') }}</th>
                    <th scope="col">{{ __('Réponse 4') }}</th>
                    <th scope="col">{{ __('Bonne(s) réponse(s)') }}</th>
                    <th scope="col">{{ __('Created_at') }}</th>
                    <th scope="col">{{ __('creator') }}</th>
                    <th scope="col">{{ __('Action') }}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($questions as $question)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $question->quest }}</td>
                    <td>{{ $question->rep1 }}</td>
                    <td>{{ $question->rep2 }}</td>
                    <td>{{ $question->rep3 }}</td>
                    <td>{{ $question->rep4 }}</td>
                    <td>{{ $question->brep }}</td>
                    <td>{{ $question->created_at }}</td>
                    <td>{{ $question->creator_email }}</td>
                    <td class="btn-group">
                        <a class="btn btn-sm btn-outline-secondary" href="{{ route('form-edit-question', $question->id) }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil" viewBox="0 0 16 16">
                                <path d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5 13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                            </svg>
                        </a>
                        <a class="btn btn-sm btn-outline-danger" href="{{ route('destroy-question', $question->id) }}">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3" viewBox="0 0 16 16">
                                <path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5ZM11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H2.506a.58.58 0 0 0-.01 0H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1h-.995a.59.59 0 0 0-.01 0H11Zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5h9.916Zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47ZM8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5Z"/>
                            </svg>
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <div class="mt-5 mb-3">{{$questions->onEachSide(1)->links()}}</div>

    </div>
</div>
@endsection
