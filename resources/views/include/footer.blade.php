<footer class="container text-center" style="padding: 2.5rem 0;color: #727272;background-color: #f9f9f9;border-top: .05rem solid #e5e5e5;">
    <p>{{ __('Website created by Adama NIADA (a Freelancer)') }} &middot; &copy; {{ __('2022') }}</p>
    <p><a href="#">{{ __('Haut de la page') }}</a></p>
</footer>