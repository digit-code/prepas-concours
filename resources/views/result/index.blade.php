@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row g-3 mb-3">
        <div class="col-md-12">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger" role="alert">
                    {{ session('error') }}
                </div>
            @endif
        </div>


        <div class="col-md-12">
            <div class="card shadow-sm">
                <div class="card-body">
                    <div class="card-text">
                        <span>{{ __('Légende') }} {{ __(':') }}</span>
                        <span class="badge bg-primary">{{ __('bonne reponse non selectionnée') }}</span>
                        <span class="badge bg-success">{{ __('bonne reponse selectionnée') }}</span>
                        <span class="badge bg-danger">{{ __('Mauvaise reponse selectionnée') }}</span>
                    </div>
                </div>
            </div>
        </div>

        @foreach ($results as $question)
            <div class="col-md-12">
                <div class="card shadow-sm">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        <h6 class="my-0">{{ $question->quest }}</h6>
                        <div>
                            <span class="text-danger">{{ $loop->iteration }}</span>
                            <span>{{ __('/') }}</span>
                            <span>{{ $loop->count }}</span>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3 mb-1">
                                @if($question->rep1 != NULL)
                                    @if((str_contains($question->brep, 'a') == TRUE) && str_contains($question->choix, 'a') == TRUE)
                                        <div class=" rounded bg-success text-white p-2">
                                            <span>{{ ('a)') }}</span>
                                            <span>{{ $question->rep1 }}</span>
                                        </div>
                                    @endif
                                    @if((str_contains($question->brep, 'a') == TRUE) && str_contains($question->choix, 'a') == FALSE)
                                        <div class=" rounded bg-primary text-white p-2">
                                            <span>{{ ('a)') }}</span>
                                            <span>{{ $question->rep1 }}</span>
                                        </div>
                                    @endif
                                    @if((str_contains($question->brep, 'a') == FALSE) && str_contains($question->choix, 'a') == TRUE)
                                        <div class=" rounded bg-danger text-white p-2">
                                            <span>{{ ('a)') }}</span>
                                            <span>{{ $question->rep1 }}</span>
                                        </div>
                                    @endif
                                    @if((str_contains($question->brep, 'a') == FALSE) && str_contains($question->choix, 'a') == FALSE)
                                        <div class=" rounded p-2">
                                            <span>{{ ('a)') }}</span>
                                            <span>{{ $question->rep1 }}</span>
                                        </div>
                                    @endif
                                @endif
                            </div>
                            <div class="col-md-3 mb-1">
                                @if($question->rep2 != NULL)
                                    @if((str_contains($question->brep, 'b') == TRUE) && str_contains($question->choix, 'b') == TRUE)
                                        <div class=" rounded bg-success text-white p-2">
                                            <span>{{ ('b)') }}</span>
                                            <span>{{ $question->rep2 }}</span>
                                        </div>
                                    @endif
                                    @if((str_contains($question->brep, 'b') == TRUE) && str_contains($question->choix, 'b') == FALSE)
                                        <div class=" rounded bg-primary text-white p-2">
                                            <span>{{ ('b)') }}</span>
                                            <span>{{ $question->rep2 }}</span>
                                        </div>
                                    @endif
                                    @if((str_contains($question->brep, 'b') == FALSE) && str_contains($question->choix, 'b') == TRUE)
                                        <div class=" rounded bg-danger text-white p-2">
                                            <span>{{ ('b)') }}</span>
                                            <span>{{ $question->rep2 }}</span>
                                        </div>
                                    @endif
                                    @if((str_contains($question->brep, 'b') == FALSE) && str_contains($question->choix, 'b') == FALSE)
                                        <div class=" rounded p-2">
                                            <span>{{ ('b)') }}</span>
                                            <span>{{ $question->rep2 }}</span>
                                        </div>
                                    @endif
                                @endif
                            </div>
                            <div class="col-md-3 mb-1">
                                @if($question->rep3 != NULL)
                                    @if((str_contains($question->brep, 'c') == TRUE) && str_contains($question->choix, 'c') == TRUE)
                                        <div class=" rounded bg-success text-white p-2">
                                            <span>{{ ('c)') }}</span>
                                            <span>{{ $question->rep3 }}</span>
                                        </div>
                                    @endif
                                    @if((str_contains($question->brep, 'c') == TRUE) && str_contains($question->choix, 'c') == FALSE)
                                        <div class=" rounded bg-primary text-white p-2">
                                            <span>{{ ('c)') }}</span>
                                            <span>{{ $question->rep3 }}</span>
                                        </div>
                                    @endif
                                    @if((str_contains($question->brep, 'c') == FALSE) && str_contains($question->choix, 'c') == TRUE)
                                        <div class=" rounded bg-danger text-white p-2">
                                            <span>{{ ('c)') }}</span>
                                            <span>{{ $question->rep3 }}</span>
                                        </div>
                                    @endif
                                    @if((str_contains($question->brep, 'c') == FALSE) && str_contains($question->choix, 'c') == FALSE)
                                        <div class=" rounded p-2">
                                            <span>{{ ('c)') }}</span>
                                            <span>{{ $question->rep3 }}</span>
                                        </div>
                                    @endif
                                @endif
                            </div>
                            <div class="col-md-3 mb-1">
                                @if($question->rep4 != NULL)
                                    @if((str_contains($question->brep, 'd') == TRUE) && str_contains($question->choix, 'd') == TRUE)
                                        <div class=" rounded bg-success text-white p-2">
                                            <span>{{ ('d)') }}</span>
                                            <span>{{ $question->rep4 }}</span>
                                        </div>
                                    @endif
                                    @if((str_contains($question->brep, 'd') == TRUE) && str_contains($question->choix, 'd') == FALSE)
                                        <div class=" rounded bg-primary text-white p-2">
                                            <span>{{ ('d)') }}</span>
                                            <span>{{ $question->rep4 }}</span>
                                        </div>
                                    @endif
                                    @if((str_contains($question->brep, 'd') == FALSE) && str_contains($question->choix, 'd') == TRUE)
                                        <div class=" rounded bg-danger text-white p-2">
                                            <span>{{ ('d)') }}</span>
                                            <span>{{ $question->rep4 }}</span>
                                        </div>
                                    @endif
                                    @if((str_contains($question->brep, 'd') == FALSE) && str_contains($question->choix, 'd') == FALSE)
                                        <div class=" rounded p-2">
                                            <span>{{ ('d)') }}</span>
                                            <span>{{ $question->rep4 }}</span>
                                        </div>
                                    @endif
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <h6 class="my-0">
                            <span>{{ __('Bonnes réponses : ') }}</span>
                            @if(str_contains($question->brep, 'a'))
                                <span> {{ __('a') }} </span>
                            @endif
                            @if(str_contains($question->brep, 'b'))
                                <span> {{ __('b') }} </span>
                            @endif
                            @if(str_contains($question->brep, 'c'))
                                <span> {{ __('c') }} </span>
                            @endif
                            @if(str_contains($question->brep, 'd'))
                                <span> {{ __('d') }} </span>
                            @endif
                        </h6>
                    </div>
                </div>
            </div>
        @endforeach

        <div class="col-md-12">
            <a class="w-100 btn btn-primary" href="{{ route('home') }}">{{ __('SUIVANT') }}</a>
        </div>

    </div>


</div>
@endsection
