@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger" role="alert">
                    {{ session('error') }}
                </div>
            @endif
        </div>
    </div>
    <div class="p-4 p-md-5 mb-4 rounded text-bg-dark">
        <div class="col-md-6 px-0">
            <h1 class="display-4 fst-italic">{{('Bienvenue sur notre plateforme')}}</h1>
            <p class="lead my-3">{{ __("Bienvenus sur notre plateforme. Face aux difficultés d'obtention des surtout direct, nous avons mis en place cette plateforme dans le but d'accompagner ces diferents candidats") }}</p>
            <p class="lead mb-0">
                <a href="{{ route('home') }}" class="text-white fw-bold">{{ __('Demarrer') }}</a>
            </p>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-8 g-4">
            <div class="row">
                <div class="col-md-12 mb-3">
                    <div class="alert alert-success" role="alert">
                        <strong>{{ __('Consignes : ') }}</strong>
                        <span>{{ __('Veuillez cochez la(les) case(s) correspondant(s) à la(aux) bonne(s) reponse(s) puis soummettez vos réponses') }}</span>
                    </div>
                </div>

                <form method="GET" action="{{ route('process') }}">
                    @foreach ($questions as $question)
                        <div class="col-md-12 mb-3">
                            <div class="card">
                                <div class="card-header d-flex justify-content-between align-items-center">
                                    <div>{{ $question->quest }}</div>
                                    <div>
                                        <small class="text-danger fw-bolder">{{ $loop->iteration }}</small>
                                        <small>/</small>
                                        <small class="fw-bolder">{{ $loop->count }}</small>
                                    </div>
                                </div>

                                <div class="card-body">
                                    <div class="row">
                                        @if($question->rep1 != NULL)
                                            <div class="col-sm-3">
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" name="rep[{{ $question->id }}][]" value="a">
                                                    <label class="form-check-label" for="rep1">{{ $question->rep1 }}</label>
                                                </div>
                                            </div>
                                        @endif
                                        @if($question->rep2 != NULL)
                                            <div class="col-sm-3">
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" name="rep[{{ $question->id }}][]" value="b">
                                                    <label class="form-check-label" for="rep1">{{ $question->rep2 }}</label>
                                                </div>
                                            </div>
                                        @endif
                                        @if($question->rep3 != NULL)
                                            <div class="col-sm-3">
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" name="rep[{{ $question->id }}][]" value="c">
                                                    <label class="form-check-label" for="rep1">{{ $question->rep3 }}</label>
                                                </div>
                                            </div>
                                        @endif
                                        @if($question->rep4 != NULL)
                                            <div class="col-sm-3">
                                                <div class="form-check">
                                                    <input type="checkbox" class="form-check-input" name="rep[{{ $question->id }}][]" value="d">
                                                    <label class="form-check-label" for="rep1">{{ $question->rep4 }}</label>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <div class="col-md-8 mb-3">
                        <nav class="blog-pagination" aria-label="Pagination">
                            <button type="submit" class="btn btn-outline-primary rounded-pill" href="#">{{ __('Soumettre') }}</button>
                            <button type="reset" class="btn btn-outline-secondary rounded-pill">{{ __('Effacer') }}</button>
                        </nav>
                    </div>
                </form>

            </div>
        </div>

        @include('include.commentaire')

    </div>

</div>
@endsection
