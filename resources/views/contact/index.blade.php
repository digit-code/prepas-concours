@extends('layouts.app2')

@section('content')
<div class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __("Section contact (Messages)") }}</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
            <div class="dropdown mx-2">
                <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle" data-bs-toggle="dropdown">
                    <span data-feather="calendar" class="align-text-bottom"></span>
                        {{('Trier par')}}
                </button>
                <ul class="dropdown-menu">
                    <li>
                        <form method="GET" action="{{ route('user') }}">
                            <input type="hidden" name="sort" value="nom">
                            <button type="submit" class="dropdown-item w-100 btn btn-light">{{ __('Message lu') }}</button>
                        </form>
                    </li>
                    <li>
                        <form method="GET" action="{{ route('user') }}">
                            <input type="hidden" name="sort" value="email">
                            <button type="submit" class="dropdown-item w-100 btn btn-light">{{ __('Address mail') }}</button>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger" role="alert">
            {{ session('error') }}
        </div>
    @endif

    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
                <tr>
                    <th scope="col">{{ __('#') }}</th>
                    <th scope="col">{{ __('Address Mail') }}</th>
                    <th scope="col">{{ __('Sujet') }}</th>
                    <th scope="col">{{ __('Message') }}</th>
                    <th scope="col">{{ __('Copy') }}</th>
                    <th scope="col">{{ __('Envoyer_le') }}</th>
                    {{-- <th scope="col">{{ __('Action') }}</th> --}}
                </tr>
            </thead>
            <tbody>
                @foreach($contacts as $contact)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $contact->email }}</td>
                    <td>{{ $contact->sujet }}</td>
                    <td>{{ $contact->msg }}</td>
                    <td>{{ $contact->mailMsg }}</td>
                    <td>{{ \Carbon\Carbon::parse($contact->created_at)->formatLocaliZed('%Y-%m-%d') }} {{ __('à') }} {{ \Carbon\Carbon::parse($contact->created_at)->formatLocaliZed('%H:%M:%S') }}</td>
                    {{-- <td class="btn-group">
                        <a class="btn btn-sm btn-outline-secondary" href="{{ route('show-historique', $contact->id) }}">
                            View
                        </a>
                    </td> --}}
                </tr>
                @endforeach
            </tbody>
        </table>

        <div class="mt-5 mb-3">{{$contacts->onEachSide(1)->links()}}</div>


    </div>
</div>
@endsection
