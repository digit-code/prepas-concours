@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12 mb-3">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger" role="alert">
                    {{ session('error') }}
                </div>
            @endif
        </div>

        <div class="col-md-12 mb-3">
            <div class="row">
                <div class="col-md-8 col-lg-8">
                    <h4 class="mb-3">{{ __('Formulaire de contact') }}</h4>
                    <form method="POST" action="{{ route('store-contact-data') }}">
                        @csrf
                        <div class="row g-3 mb-3">
                            <div class="col-sm-6">
                                <label for="email" class="form-label">{{ __('Address Mail') }}</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                            </div>
        
                            <div class="col-sm-6">
                                <label for="sujet" class="form-label">{{ __('sujet du message') }}</label>
                                <input id="sujet" type="text" class="form-control @error('sujet') is-invalid @enderror" name="sujet" value="{{ old('sujet') }}" required autocomplete="sujet" autofocus>
                            </div>
        
                            <div class="col-sm-12">
                                <label for="msg" class="form-label">{{ __('contenu du message') }}</label>
                                <textarea id="msg" class="form-control @error('msg') is-invalid @enderror" name="msg" value="{{ old('msg') }}" rows="3" required autocomplete="msg" autofocus></textarea>
                            </div>
        
                            <div class="col-sm-12">
                                <div class="form-check">
                                    <input id="mailMsg" type="checkbox" class="form-check-input @error('mailMsg') is-invalid @enderror" name="mailMsg" value="1" autocomplete="mailMsg" autofocus>
                                    <label class="form-check-label" for="save-info">{{ __('Voulez vous recevoir une copie de votre message par mail ?') }}</label>
                                </div>
                            </div>
                        </div>

                        <div class="mb-3">
                            <button class="w-100 btn btn-primary btn-lg" type="submit" @disabled($errors->isNotEmpty())>{{__('NOUS CONTACTEZ')}}</button>
                        </div>
                    </form>
                </div>

                @include('include.commentaire')

            </div>
        </div>

    </div>
</div>
@endsection
