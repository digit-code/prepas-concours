@extends('layouts.app2')

@section('content')
<div class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">{{ __("Historique de connexion des utilisateurs") }}</h1>
    </div>

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger" role="alert">
            {{ session('error') }}
        </div>
    @endif

    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
                <thead>
                    <tr>
                        <th scope="col">{{ __('#') }}</th>
                        <th scope="col">{{ __('Address Mail') }}</th>
                        <th scope="col">{{ __('Address ip') }}</th>
                        <th scope="col">{{ __('Devise') }}</th>
                        <th scope="col">{{ __('Address mac') }}</th>
                        <th scope="col">{{ __('Derniere connexion') }}</th>
                        <th scope="col">{{ __('Date de deconnexion') }}</th>
                        <th scope="col">{{ __('Action') }}</th>
                    </tr>
                </thead>
            </thead>
            <tbody>
                @foreach($logs as $log)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $log->email }}</td>
                        <td>{{ $log->ip_address }}</td>
                        <td>{{ $log->devise }}</td>
                        <td>{{ $log->mac_address }}</td>
                        <td>{{ $log->created_at }}</td>
                        <td>
                            @if ($log->logout_at == NULL)
                                <span class="text-danger">{{ __('online') }}</span>
                            @else
                                {{ $log->logout_at }}
                            @endif
                        </td>
                        <td class="btn-group">
                            <a class="btn btn-sm btn-outline-secondary" href="{{ route('show-historique-de-connexion', $log->user_id) }}">
                                voire
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <div class="mt-5 mb-3">{{$logs->onEachSide(1)->links()}}</div>


    </div>
</div>
@endsection
