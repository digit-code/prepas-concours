@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger" role="alert">
                    {{ session('error') }}
                </div>
            @endif
        </div>

        <div class="col-sm-12">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ __('Consignes : ') }}</strong>
                {{-- <a href="#" class="alert-link">an example link</a>. Give it a click if you like. --}}
                <span>{{ __('Veuillez cochez la(les) case(s) correspondant(s) à la(aux) bonne(s) reponse(s) puis soummettez vos réponses') }}</span>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        </div>

        <form action="{{ route('process') }}">
            <div class="row">

                @foreach ($questions as $question)
                <div class="col-sm-4 mb-3">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <div>{{ $question->quest }}</div>
                            <div>
                                <small class="text-danger fw-bolder">{{ $loop->iteration }}</small>
                                <small>/</small>
                                <small class="fw-bolder">{{ $loop->count }}</small>
                            </div>
                        </div>
                        <div class="card-body">
                            @if($question->rep1 != NULL)
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="rep[{{ $question->id }}][]" value="a">
                                    <label class="form-check-label" for="rep1">{{ $question->rep1 }}</label>
                                </div>
                            @endif
                            @if($question->rep2 != NULL)
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="rep[{{ $question->id }}][]" value="b">
                                    <label class="form-check-label" for="rep1">{{ $question->rep2 }}</label>
                                </div>
                            @endif
                            @if($question->rep3 != NULL)
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="rep[{{ $question->id }}][]" value="c">
                                    <label class="form-check-label" for="rep1">{{ $question->rep3 }}</label>
                                </div>
                            @endif
                            @if($question->rep4 != NULL)
                                <div class="form-check">
                                    <input type="checkbox" class="form-check-input" name="rep[{{ $question->id }}][]" value="d">
                                    <label class="form-check-label" for="rep1">{{ $question->rep4 }}</label>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                @endforeach

                <div class="col-sm-12 d-flex justify-content-between align-items-center mb-3">
                    <div></div>
                    <div>
                        <button type="submit" class="btn btn-outline-success">{{ __('Sommettre') }}</button>
                        <button type="reset" class="btn btn-outline-secondary">{{ __('Renitialiser') }}</button>
                    </div>
                </div>

            </div>
        </form>

    </div>
</div>
@endsection
