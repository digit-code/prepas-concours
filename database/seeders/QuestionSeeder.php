<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\Question;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    use WithoutModelEvents;

    public function run() : void
    {
        for ($i=0; $i < 100 ; $i++) { 
            DB::table('questions')->insert([
                'user_id' => 1,
                'concours' => 'Direct',
                'question_de' => 'Culture Generale',
                'quest' => fake()->address(),
                'rep1' => fake()->streetName(),
                'rep2' => fake()->streetAddress(),
                'rep3' => fake()->postcode(),
                'rep4' => fake()->country(),
                'brep' => json_encode([fake()->randomElement($array = ['a', 'b']), fake()->randomElement($array = ['c', 'd'])]),
            ]);
        }
    }
}
